package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"github.com/jackmanlabs/bucket/jlog"
	"github.com/jackmanlabs/errors"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func main() {

	var (
		root *string = flag.String("root", ".", "The root folder of the git repository you'd like to examine. Defaults to present working directory.")
	)

	flag.Parse()

	root_, err := filepath.Abs(*root)
	if err != nil {
		flag.Usage()
		log.Fatal(errors.Stack(err))
	}

	log.Print("Repository: ", root_)

	commits, err := findCommits(root_)
	if err != nil {
		flag.Usage()
		log.Fatal(errors.Stack(err))
	}

	commitTree := organizeCommits(commits)

	jlog.Log(commitTree)
}

func findCommits(root string) (map[string]*Commit, error) {

	b := bytes.NewBuffer(nil)

	cmd := exec.Command(
		"git",
		"--no-pager",
		"log",
		"--format=format:%H:%at:%P",
	)
	cmd.Dir = root
	cmd.Stderr = os.Stderr
	cmd.Stdout = b

	err := cmd.Run()
	if err != nil {
		return nil, errors.Stack(err)
	}

	commits := make(map[string]*Commit)

	scnr := bufio.NewScanner(b)
	for scnr.Scan() {
		line := scnr.Text()
		chunks := strings.Split(line, ":")
		if len(chunks) != 3 {
			continue
		}

		ts, err := strconv.ParseInt(chunks[1], 10, 64)
		if err != nil {
			return nil, errors.Stack(err)
		}

		date := time.Unix(ts, 0)

		parents := strings.Split(chunks[2], " ")

		var c *Commit = &Commit{
			Root:         root,
			Hash:         chunks[0],
			Date:         date,
			SqlFiles:     make(map[string][]byte),
			ParentHashes: make([]string,0),
		}

		for _, parent := range parents{
			if parent != ""{
				c.ParentHashes = append(c.ParentHashes, parent)
			}
		}

		err = c.findSqlFiles()
		if err != nil {
			log.Fatal(errors.Stack(err))
		}

		commits [c.Hash] = c
	}

	err = scnr.Err()
	if err != nil {
		return nil, errors.Stack(err)
	}

	return commits, nil
}

func organizeCommits(commits map[string]*Commit) *Commit {
	var root *Commit

	for _, commit := range commits {
		if len(commit.ParentHashes) == 0 {
			root = commit
		}
	}

	root.Children = findChildren(root.Hash, commits)

	return root
}

func findChildren(parentHash string, potentialChildren map[string]*Commit) []*Commit {

	log.Print("Finding children for ", parentHash)

	children := make([]*Commit, 0)

	for _, commit := range potentialChildren {
		if sContains(commit.ParentHashes, parentHash) {
			children = append(children, commit)
		}
	}

	for _, child := range children {
		delete(potentialChildren,child.Hash)
	}


	for _, child := range children {
		child.Children = findChildren(child.Hash, potentialChildren)
	}

	return children
}


func sContains(set []string, s string) bool {
	for _, s_ := range set {
		if s_ == s {
			return true
		}
	}

	return false
}

type Commit struct {
	Root         string
	Hash         string
	Date         time.Time
	SqlFiles     map[string][]byte
	ParentHashes []string
	Children     []*Commit
}

func (this *Commit) findSqlFiles() error {

	b := bytes.NewBuffer(nil)

	cmd := exec.Command(
		"git",
		"--no-pager",
		"diff-tree",
		"--no-commit-id",
		"--name-status",
		"-r",
		this.Hash,
	)
	cmd.Dir = this.Root
	cmd.Stderr = os.Stderr
	cmd.Stdout = b

	err := cmd.Run()
	if err != nil {
		return errors.Stack(err)
	}

	scnr := bufio.NewScanner(b)
	for scnr.Scan() {
		line := scnr.Text()

		chunks := strings.Split(line, "\t")
		if len(chunks) != 2 {
			log.Print(line)
			continue
		}

		status := chunks[0]
		path := chunks[1]

		if status == "D" {
			continue
		}

		if strings.HasSuffix(line, ".sql") {
			log.Printf("Hash: %s\tFile: %s", this.Hash, path)
			contents, err := loadFile(this.Root, this.Hash, path)
			if err != nil {
				return errors.Stack(err)
			}
			this.SqlFiles[path] = contents
		}
	}

	err = scnr.Err()
	if err != nil {
		return errors.Stack(err)
	}

	return nil
}

func loadFile(root, hash, path string) ([]byte, error) {
	b := bytes.NewBuffer(nil)

	cmd := exec.Command(
		"git",
		"--no-pager",
		"show",
		fmt.Sprintf("%s:%s", hash, path),
	)

	cmd.Dir = root
	cmd.Stderr = os.Stderr
	cmd.Stdout = b

	err := cmd.Run()
	if err != nil {
		jlog.Log(cmd.Args)
		return nil, errors.Stack(err)
	}

	return b.Bytes(), nil
}
